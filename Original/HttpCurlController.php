<?php

/**
 * Class HttpCurlController
 * This calss will be used to to init the client
 * Generic http client method for third part API calling
 * @package DTApi\Repository
 */
class HttpCurlController
{
    // Hold the class instance.
    private static $instance = null;
    public static $ch = null;
    // The constructor is private
    // to prevent initiation with outer code.
    private function __construct()
    {
        $ch = curl_init();
    }

    // The object is created from within the class itself
    // only if the class has no instance.
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Singleton();
        }

        return self::$instance;
    }
    public function HttpClient($url, $authkey)
    {
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authkey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        return $ch;
    }
}
